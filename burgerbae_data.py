i=1
from pymongo import MongoClient
import time
import datetime as dt
import pandas as pd
from dotenv import dotenv_values
import os

config = dotenv_values(".env") 

URL=os.getenv('URL')
DB=os.getenv('DB')
COLL=os.getenv('COLL')
INTERVAL=1200
while(i==1):
	from pymongo import MongoClient

	from pymongo import MongoClient

	# Requires the PyMongo package.
	# https://api.mongodb.com/python/current

	from pymongo import MongoClient

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

	client = MongoClient('mongodb://dl-user-1:A2mClcihXGlNQC7M@datalake0-3comg.a.query.mongodb.net/Database0?ssl=true&authSource=admin')
	result = client['Database0']['prod_order.order'].aggregate([
		{
			'$lookup': {
				'from': 'brand', 
				'localField': 'brand_id', 
				'foreignField': '_id', 
				'as': 'brand_info'
			}
		}, {
			'$unwind': {
				'path': '$items'
			}
		}, {
			'$unwind': {
				'path': '$brand_info'
			}
		}, {
			'$match': {
				'brand_info.name': 'Burger Bae'
			}
		}, {
			'$match': {
				'order_status.code': {
					'$nin': [
						'initiated'
					]
				}
			}
		}, {
			'$match': {
				'order_status.code': {
					'$nin': [
						'failed'
					]
				}
			}
		}, {
			'$sort': {
				'_id': -1
			}
		}, {
			'$project': {
				'_id': 0, 
				'Order Number': '$order_id', 
				'Order Date': '$created_at', 
				'Name': '$shipping_address.display_name',  
				'Address': '$shipping_address.line1', 
				'City': '$shipping_address.city', 
				'State': '$shipping_address.state.name', 
				'Pincode': '$shipping_address.postal_code', 
				'Country': '$shipping_address.country.name', 
				'Email': ' ', 
				'Phone Number': '$shipping_address.phone_number.number', 
				'Total Price': '$items.total_price.value', 
				'Offer Value': '$items.offer_value.value', 
				'Item Details': '$items.catalog_info.name', 
				'Quantity': '$items.quantity', 
				'HSN Code': ' ', 
				'Payment Method': '$payment_mode', 
				'Shipping Amount': ' ', 
				'SKU': '$items.catalog_info.variant.sku', 
				'Order Status': '$order_status.code'
			}
		}
	])

	df = pd.DataFrame(result)
	df["Offer Value"] = df["Offer Value"].fillna(0) 
	df['Item Cost']=df['Total Price']-df['Offer Value']
	def payment_check(x):
			if x!= "cod":
				return "prepaid"
			else:
				return 'cod'

	df["First Name"] = df['Name'].apply(lambda x: x.split()[0])
	df["Last Name"] = df['Name'].apply(lambda x:  " ".join(x.split()[1:]))
	df['Order Date'] = df['Order Date'].apply(lambda x: x + dt.timedelta(hours=5, minutes=30))
	df['Order Date'] = df['Order Date'].apply(lambda x: x.strftime("%d-%m-%Y"))
	df['Payment Method'] = df['Payment Method'].apply(payment_check)
	df['Item Details'] = df['Item Details'] + ", " + df['SKU']
	df = df.drop('SKU', axis = 1)
	df = df.drop('Name', axis = 1)
	df = df[['Order Number','Order Date','First Name', 'Last Name', 'Address', 'City', 'State', 'Pincode','Country','Email','Phone Number','Item Cost', 'Item Details', 'Quantity','HSN Code', 'Payment Method', 'Shipping Amount', "Order Status" ]]
	df.to_csv("/home/ubuntu/charts_by_srishti/burgerbae.csv")

	time.sleep(INTERVAL)
