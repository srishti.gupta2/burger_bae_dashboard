import datetime as dt
import copy
import flask
import dash
import datetime as dt
from dash import dcc
import dash_bootstrap_components as dbc
from dash import html
from dash import dash_table
from dash.dependencies import Input, Output
import pandas as pd

def init_dash_app(requests_pathname_prefix, server):
    app = dash.Dash(__name__, requests_pathname_prefix=requests_pathname_prefix, server=server)
    app.scripts.config.serve_locally = False
    dcc._js_dist[0]['external_url'] = 'https://cdn.plot.ly/plotly-basic-latest.min.js'
    df=pd.read_csv("./burgerbae.csv")
    df = df.drop('Unnamed: 0',axis=1)

    app.layout = html.Div([
        dcc.Markdown('# Burger Bae Orders', style={'textAlign':'center', 'font-family': 'arial', 'fontSize': '10px'}),

        dash_table.DataTable(
            id='datatable-interactivity',
            columns=[
                {"name": i, "id": i, "deletable": True, "selectable": True} for i in df.columns
            ],
            data=df.to_dict('records'),
            export_format="csv",
            export_headers="display",
            style_cell={'textAlign': 'left', 'padding': '5px', 'minWidth': 170, 'width': 250, 'maxWidth': 250, 'font-family': 'arial', 'fontSize': '12px', 'border': '1px solid rgb(211, 211, 211)' },
            style_as_list_view=True,
            style_header={
                'fontWeight': 'bold'
                },
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)',
                    'if': {'column_id': 'Order ID'},
                }
            ],
            virtualization = True,
            fixed_rows = {'headers' : True},
            style_table={'height': "100%", 'border-radius': 10, 'box-shadow': 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px', 'marginTop': '10px', 'border': '1px solid rgb(211, 211, 211)'},
            editable = False,
            filter_action = "native",
            sort_action = 'native',
            sort_mode = 'multi',
            column_selectable = False,
            row_deletable = False,
            selected_columns=[],
            selected_rows=[],
            page_action="native",

        ),
        
        html.Div(id='datatable-interactivity-container', style = {'margin': '10px'})
    ])

    @app.callback(
        Output('datatable-interactivity', 'data'),
    )
    def update_data(start_date, end_date):

        df_temp = copy.deepcopy(df)
        ctx = dash.callback_context
        changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]

        return df_temp.to_dict('records')

    return app

def burgerbae(requests_pathname_prefix: str = None) -> dash.Dash:
    server = flask.Flask(__name__)
    app = init_dash_app(requests_pathname_prefix, server)
    return app